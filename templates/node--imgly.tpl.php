<?php
/**
 * @file
 * Imgly's theme implementation to display an image node.
 */
// krumo($node);
?>
<div id="node-<?php print $node->nid; ?>" class="imgly-node <?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php if ($page): ?>

  <?php else: ?>
    <?php print render($title_prefix); ?>
    <h2<?php print $title_attributes; ?>>
      <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
    </h2>
    <?php print render($title_suffix); ?>

    <?php if ($display_submitted): ?>
      <div class="meta submitted">
        <?php print $submitted; ?>
      </div>
    <?php endif; ?>

    <div class="content clearfix"<?php print $content_attributes; ?>>
      <?php
        // We hide the comments and links now so that we can render them later.
        hide($content['comments']);
        hide($content['links']);
        print render($content);
      ?>
    </div>

  <?php endif; ?>

  <div class="imgly-footer clearfix">
    <h2><?php print $title; ?></h2>
  </div>

</div>