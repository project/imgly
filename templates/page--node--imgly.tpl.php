<?php
/**
 * @file
 * Imgly's theme implementation to display a single Drupal page when viewing an image node.
 */
?>
<div id="imgly-page-wrapper">

  <div id="imgly-main" class="clearfix">

    <?php print render($page['content']); ?>

  </div> <!-- /#imgly-main -->

</div> <!-- /#imgly-page-wrapper -->